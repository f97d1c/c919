Satcom::Engine.routes.draw do
  resources :restful, only:[] do 
    collection do
      get :example
      get :exposed_api
      get :request_api_params
      post :request_api
    end
  end
  
  match '/restful/request_api' => "restful#request_api", via: :options
end
