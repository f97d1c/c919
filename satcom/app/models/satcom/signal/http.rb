module Satcom
  module Signal
    module Http
      extend self

      def get(url:, params: {}, timeout: 10, open_timeout: 10)
        get_response do 
          full_url = [url, URI.encode_www_form(params)]
          full_url.delete_if{|item| !item.present? }
          RestClient.get(full_url.join("?"))
        end
      end 

      def post(url:, headers: {}, params:, timeout: 10, open_timeout: 10)
        get_response do
          RestClient::Request.execute(method: :post,url: url, payload: params, headers: headers, timeout: timeout, open_timeout: open_timeout)
        end
      end 

      private
        def get_response
          begin
            response = yield
            return [false, "响应码异常:#{response.code}", {response: response}] unless (response.code == 200)

            hash = {code: 200}
            res = handle_body_encode(body: response.body)
            return res unless res[0]

            hash.merge!({body: res[1]})
          rescue
            return [false, '请求异常', {errors: {message: $!.to_s, path: $@}}]
          end
          
          [true, hash]
        end 

        def handle_body_encode(body: )
          body = case body.encoding.to_s
          when 'UTF-8', 'ASCII-8BIT'
            body
          else
            body.encode('utf-8','gbk', :undef => :replace, :replace => "?", :invalid => :replace).chars.select{|i| i.valid_encoding?}.join
          end

          [true, body]
        end 
    end  
  end  
end