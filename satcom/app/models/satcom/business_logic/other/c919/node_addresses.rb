module Satcom
  module BusinessLogic  
    module Other
      module C919
        module NodeAddresses
          extend self

          def blueprint
            method_name = '可用节点'
            explain = '返回所有节点的可用地址信息'
            realizations = [:gather]

            data_structure = get_nodes.map{|key, value|[key, value[:name]]}.to_h
            
            described = BlueprintDescribed.new(bind: binding)
            described.get_described
          end 

          def gather(show_how: false, code: 'code', **args)
            supplier_name = '自有节点'
            source_url = "192.168.8.133"
            source_data_handel_method='自主维护'

            data_structure = get_nodes.map do |key, value|
              [key, lambda{|hash| hash[key]}]
            end.to_h

            described = RealizationDescribed.new(bind: binding)
            self_described = described.get_described
            return self_described if show_how
            
            [true, get_nodes]
          end

          private
            def get_nodes
              return @nodes if @nodes
              file_path = "public/satcom/other/c919/node_addresses.json"
              @nodes = (YAML.load(File.open(Rails.root.join(file_path))).deep_symbolize_keys rescue {})
            end 

        end
      end
    end
  end
end
