module Satcom
  module BusinessLogic  
    module Other
      module TestData
        module People
          extend self

          def blueprint
            method_name = '虚构人物数据'
            explain = '提供批量的人物测试数据'
            realizations = [:fictitious]

            data_structure = data_structure = {
              name: '姓名',
              gender: '性别',
              birthday: '出生日期',
              age: '年龄',
              address: '住址',
              id_number: '身份证号',
            }
            
            described = BlueprintDescribed.new(bind: binding)
            described.set_attribute(key: 'data_size', name: '数据条数', explain: '所需数据条数,最大值为500', example: nil, default: 10, max: 500, value_type: :integer, necessary: false)
            described.get_described
          end 

          def fictitious(show_how: false, **args)
            supplier_name = '自有节点'
            source_url = "192.168.8.133"
            source_data_handel_method='自主维护'

            data_structure = {
              name: lambda{|hash| hash[:name]},
              gender: lambda{|hash| hash[:gender]},
              birthday: lambda{|hash| hash[:birthday]},
              age: lambda{|hash| hash[:age]},
              address: lambda{|hash| hash[:address]},
              id_number: lambda{|hash| hash[:id_number]},
            }

            described = RealizationDescribed.new(bind: binding)
            self_described = described.get_described
            return self_described if show_how

            tmp = []
            args[:data_size].times do 
              tmp << virtual_identity
            end
            
            [true, tmp]
          end

          private
            def virtual_identity
              birthday = rand_date(70*365)
              age = ((Time.now.to_i - birthday.beginning_of_day.to_i) / (60*60*24*365)).to_i+1
              gender = ['男', '女'].sample
              name =  rand_name(gender: gender)
              res = rand_address
              address = res[0]
              arrdess_code = res[1]
              id_number = rand_id_number(arrdess_code: arrdess_code, birthday: birthday)

              {name: name, gender: gender, birthday: birthday.to_s, age: age, address: address, id_number: id_number}
            end

            def rand_date(days)
              rand(days).days.ago(Date.today)
            end

            def rand_name(gender: )

              unless @surname
                file_path = 'public/satcom/other/test_data/people/surname.json'
                @surname = YAML.load(File.open(Rails.root.join(file_path)))
              end

              unless @boys_name
                file_path = 'public/satcom/other/test_data/people/boys_name.json'
                @boys_name = YAML.load(File.open(Rails.root.join(file_path)))
              end

              unless @girls_name
                file_path = 'public/satcom/other/test_data/people/girls_name.json'
                @girls_name = YAML.load(File.open(Rails.root.join(file_path)))
              end

              @surname.sample + ((gender == '女') ? @girls_name : @boys_name).sample
            end

            def rand_address
              unless @address
                file_path = 'public/satcom/other/test_data/people/address.json'
                @address = YAML.load(File.open(Rails.root.join(file_path)))
              end

              level_1 = @address.sample
              level_2 = (level_1['level_2'] || []).sample
              
              unless level_2
                return [level_1['region'], level_1['code']]
              end
              
              level_3 = (level_2['level_3'] || [{}]).sample
              full_address = "#{level_1['region']}#{level_2['region']}#{level_3['region']}"
              [full_address, (level_3['code'] || level_2['code'])]
            end 

            def rand_id_number(arrdess_code: , birthday: )
              end_str = "#{rand(100...10000)}X"[0..3]
              "#{arrdess_code}#{birthday.to_s.gsub('-','')}#{end_str}"
            end

        end
      end
    end
  end
end
