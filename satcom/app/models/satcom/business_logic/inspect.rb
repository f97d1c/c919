module Satcom
  module BusinessLogic
    # 对标准格式内容进行检查
    module Inspect
      extend self

        # 全自检项检查
        def inspect_all(self_described:, reality:)
          ideal_reality = {ideal: self_described[:attributes], reality: reality}

          res = default_value(ideal_reality)
          return res unless res[0]

          res = value_type(ideal_reality)
          return res unless res[0]
          
          res = attribute_necessary(ideal_reality)
          return res unless res[0]

          res = check_data_format(self_described: self_described, reality: reality)
          return res unless res[0]

          [true, '']
        end 

        # 参数项检查
        def attribute_necessary(ideal:, reality:)
          # 检查必须参数是否缺失
          necessary_standards = ideal.select{|standard| standard[:necessary] == true}
          necessary_standards.each do |standard|
            key = standard[:key]
            unless (reality[key.to_sym] || reality[key]).present?
              return [false, "#{standard[:name]}(#{key})为必须项,不能为空"]
            end 
          end 
          
          # 检查参数值是否符合正则式要求
          reality.each do |key, value|
            standard = ideal.select{|standard| [key, key.to_sym, key.to_s].uniq.include?(standard[:key])}[0]
            # return [false, "未找到关于参数:#{key} 的定义"] unless standard
            next unless standard

            if standard[:regular] && (value.to_s =~ standard[:regular]).nil?
              tmp = "参数:#{standard[:name]}(#{key})的参数值(#{value})不满足要求"
              tmp+= ", 请参考: #{standard[:explain]}" if standard[:explain].present?
              tmp+= ", 示例值: #{standard[:example]}" if standard[:example].present?
              tmp+= ", 默认值: #{standard[:default]}" if standard[:default].present?
              tmp+='.'
              return [false, tmp] 
            end 
          end 

          [true, '']
        end 

        # 参数默认值检查
        def default_value(ideal:, reality:)
          ideal.select{|standard| !standard[:default].nil?}.each do |standard|
            unless reality[standard[:key].to_sym].present?
              reality.merge!({standard[:key].to_sym => standard[:default]})
            end
          end 

          [true, '']
        end

        # 参数值格式检查
        def value_type(ideal:, reality:)
          ideal.each do |standard|
            unless standard[:value_type].present?
              return [false, "自描述参数标准检查异常: #{standard[:name]}(#{standard[:key]})未设置对应参数值类型(value_type)."]
            end 
          end

          ideal.each do |standard|
            old_value = reality[standard[:key].to_sym]

            case standard[:value_type].to_sym
            when :string_or_array, :array_or_string
              new_value = (JSON.parse(old_value) rescue false)
              new_value = [new_value] if new_value.is_a?(Integer)
              new_value = old_value.split(/, |,/) unless new_value
            when :array
              error_msg = [false, "参数:#{standard[:key]},参数类型不符合Json数组要求."]
              next if old_value.is_a?(Array)

              new_value = (JSON.parse(old_value) rescue (return error_msg))
              return error_msg unless new_value.is_a?(Array)
            when :key_value
              error_msg = [false, "参数:#{standard[:key]},参数类型不符合Json键值对要求."]
              next if old_value.is_a?(Hash)

              new_value = (JSON.parse(old_value) rescue (return error_msg))
              return error_msg unless new_value.is_a?(Hash)
            when :boolean
              new_value = [true, 'true', '1', 1].include?(old_value)
            when :integer
              max_value = standard[:max]
              return [false, "参数:#{standard[:key]},参数值:#{old_value} 大于接口定义最大值:#{max_value}."] if max_value.present? && (old_value.to_i > max_value)
              next if old_value.is_a?(Integer)
              if old_value == '0'
                new_value = 0
              else
                return [false, "参数:#{standard[:key]},参数类型不符合数字要求."] if ((old_value.to_i rescue 0) == 0)
                new_value = old_value.to_i
              end
            else
              next
            end 

            unless (old_value == new_value)
              reality.merge!({standard[:key].to_sym => new_value})
            end 
          end

          [true, '']
        end 

        def check_data_format(self_described:, reality:)
          data_format = {}
          if (reality[:data_format] == ['original']) && (reality[:key_type] == 'zh')
            self_described[:data_structure].map do |key, value| 
              data_format.merge!([[value, key]].to_h)
            end
            reality[:data_format] = data_format
            return [true, ''] 
          elsif (reality[:data_format] == ['original'])
            self_described[:data_structure].each do |key, value| 
              data_format.merge!([[key, key]].to_h)
            end
            reality[:data_format] = data_format
            return [true, ''] 
          end 
          
          data_format = reality[:data_format]
          data_format = data_format.map(&:to_sym)
          data_structure = {}
          self_described[:data_structure].map{|key, value| data_structure.merge!([[key, key], [value.to_sym, key]].to_h)}
          hit_items = data_structure.select{|key, value| data_format.include?(key)}
          missed = (data_format - hit_items.keys).join(',')
          return [false, "所提供数据格式中:#{missed},未找到对应匹配数据项"] if missed.present?
          
          reality[:data_format] = hit_items
          [true, '']
        end 

    end
  end
end