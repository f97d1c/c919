module Satcom
  module BusinessLogic
    # 自描述类(具体实现) 用于对接口进行定义和描述                                                 
    class RealizationDescribed
      
      def initialize(bind: )
        @bind = bind
        @self_ = eval("self", @bind)
        @method = eval("__method__", @bind)
        @attributes = []
      end

      def columns
        {
          supplier_name: '供应商名称',
          source_url: '原始数据接口地址',
          source_data_handel_method: '原始数据处理方式',
          method_path: '方法调用路径',
          data_structure: '具体实现中根据标准数据结构中的各元素提供的对应生成方法.',
        }
      end 

      def get_described
        # raise "获取自描述内容前先定义属性值(set_attribute)" unless @attributes.present?
        tmp = {}
        columns.keys.each do |column|
          method_name = "get_#{column}".to_sym
          res = if respond_to?(method_name)
            send(method_name)
          else
            get_variable.call(column)
          end

          return res unless res[0]
          tmp.merge!(res[1])
        end 
        tmp
      end 

      def get_variable
        lambda do |variable|
          variable_ = (eval(variable.to_s, @bind) rescue nil)
          return [false, "宿主方法内未定义:#{variable}变量(#{columns[variable.to_sym] || '暂无任何描述信息'})"] unless variable_
          [true, {variable => variable_}.symbolize_keys]
        end 
      end 

      def get_method_path
        method_path = (eval("method_path", @bind) rescue nil)
        return [true, {method_path: method_path}] if method_path
        method_path = lambda{|**args| @self_.send(@method, args)}
        [true, {method_path: method_path}]
      end 

      def get_attributes
        # 加载通用属性
        currency_attributes
        [true, {attributes: @attributes}]
      end 

      def set_attribute(key:, name:, explain:, regular: nil, default: '', example:, necessary: true, value_type:)
        attribute = {
          key: key,
          name: name,
          explain: explain,
          regular: regular,
          default: default,
          necessary: necessary,
          value_type: value_type
        }
        @attributes << attribute
      end 

      private

        # 通用属性
        def currency_attributes
         
        end

    end

  end 
end