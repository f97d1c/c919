module Satcom
  module BusinessLogic
    module Finance
      module Fund
        module BaseInfo
          extend self

          def blueprint
            method_name = '基本信息'
            explain = '根据基金代码返回相关信息'
            realizations = [:realization_eastmoney]

            data_structure = {
              full_name_of_fund: '基金全称',
              fund_abbreviation: '基金简称',
              fund_code: '基金代码',
              fund_type: '基金类型',
              issue_date: '发行日期',
              date_of_establishment_or_scale: '成立日期/规模',
              asset_size: '资产规模',
              share_size: '份额规模',
              fund_manager: '基金管理人',
              fund_trustee: '基金托管人',
              fund_manager: '基金经理人',
              set_up_to_pay_dividends: '成立来分红',
              management_rate: '管理费率',
              escrow_rate: '托管费率',
              sales_service_rate: '销售服务费率',
              maximum_subscription_rate: '最高认购费率',
              performance_benchmark: '业绩比较基准',
              tracking_target: '跟踪标的',
              master_or_sub_fund: '母子基金',
              pairing_conversion: '是否配对转换',
              transfer_to_lof: '是否转LOF',
              closed_period: '封闭期',
              early_termination_clause: '提前结束条款',
              fixed_conversion_date: '定期折算日',
              irregular_conversion_conditions: '不定期折算条件',
              annualized_rate_of_return_on_fixed_income_share: '固定收益份额年化收益率',
              deficit_critical_point: '亏损临界点',
              the_critical_point_of_guaranteed_return: '保收益临界点',
              critical_point_of_excess_return: '超额收益临界点',
            }
            
            described = BlueprintDescribed.new(bind: binding)
            described.set_attribute(key: 'code', name: '基金代码', explain: '', example: '161725', value_type: :string)

            described.get_described
          end 

          def realization_eastmoney(show_how: false, code: 'code', **args)
            supplier_name = '天天基金'
            source_url = "http://fundf10.eastmoney.com/jbgk_#{code}.html"
            source_data_handel_method='Html解析'

            data_structure = {
              full_name_of_fund: lambda{|hash| hash['基金全称']},
              fund_abbreviation: lambda{|hash| hash['基金简称']},
              fund_code: lambda{|hash| hash['基金代码']},
              fund_type: lambda{|hash| hash['基金类型']},
              issue_date: lambda{|hash| hash['发行日期']},
              date_of_establishment_or_scale: lambda{|hash| hash['成立日期/规模']},
              asset_size: lambda{|hash| hash['资产规模']},
              share_size: lambda{|hash| hash['份额规模']},
              fund_manager: lambda{|hash| hash['基金管理人']},
              fund_trustee: lambda{|hash| hash['基金托管人']},
              fund_manager: lambda{|hash| hash['基金经理人']},
              set_up_to_pay_dividends: lambda{|hash| hash['成立来分红']},
              management_rate: lambda{|hash| hash['管理费率']},
              escrow_rate: lambda{|hash| hash['托管费率']},
              sales_service_rate: lambda{|hash| hash['销售服务费率']},
              maximum_subscription_rate: lambda{|hash| hash['最高认购费率']},
              performance_benchmark: lambda{|hash| hash['业绩比较基准']},
              tracking_target: lambda{|hash| hash['跟踪标的']},
              master_or_sub_fund: lambda{|hash| hash['母子基金']},
              pairing_conversion: lambda{|hash| hash['是否配对转换']},
              transfer_to_lof: lambda{|hash| hash['是否转LOF']},
              closed_period: lambda{|hash| hash['封闭期']},
              early_termination_clause: lambda{|hash| hash['提前结束条款']},
              fixed_conversion_date: lambda{|hash| hash['定期折算日']},
              irregular_conversion_conditions: lambda{|hash| hash['不定期折算条件']},
              annualized_rate_of_return_on_fixed_income_share: lambda{|hash| hash['固定收益份额年化收益率']},
              deficit_critical_point: lambda{|hash| hash['亏损临界点']},
              the_critical_point_of_guaranteed_return: lambda{|hash| hash['保收益临界点']},
              critical_point_of_excess_return: lambda{|hash| hash['超额收益临界点']},
            }

            described = RealizationDescribed.new(bind: binding)
            self_described = described.get_described
            return self_described if show_how

            res = Satcom::Signal::Http.get(url: source_url)
            return res unless res[0]

            doc = Nokogiri::HTML.parse(res[1][:body])
            datas = {}
            doc.xpath("//*[@class='detail']").css('tr').each do |tr|
              th_contents = tr.css('th').map(&:text)
              td_contents = tr.css('td').map(&:text)
              tmp = {}
              th_contents.each_with_index do |th_content, index|
                tmp.merge!({th_content => td_contents[index]})
              end 
              datas.merge!(tmp)
            end 

            [true, datas]
          end 

        end 
      end
    end
  end
end