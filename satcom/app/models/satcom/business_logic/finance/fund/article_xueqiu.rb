module Satcom
  module BusinessLogic
    module Finance
      module Fund
        module ArticleXueqiu
          extend self

          def blueprint
            method_name = '雪球专栏文章解析'
            explain = '根据提供的链接, 将专栏文章转换为Markdown等文本格式'
            realizations = [:realization_xueqiu]

            # 规定标准返回数据格式
            data_structure = {
              md_content: 'Markdown格式内容'
            }

            described = BlueprintDescribed.new(bind: binding)
            described.set_attribute(key: 'article_link', name: '文章链接', explain: '雪球专栏文章链接', example: 'https://xueqiu.com/3469370889/128904217', value_type: :string)
            described.set_attribute(key: 'linefeed', name: '换行符', explain: '定义文本之间的换行符', example: '{"markdown": "<br>\n"}', value_type: :key_value)

            described.get_described
          end 

          private
            def realization_xueqiu(show_how: false, **args)
              supplier_name = '雪球'
              source_url = 'https://xueqiu.com'
              source_data_handel_method = 'html解析'
              
              # 根据主方法定义数据格式加工数据
              data_structure = {
                md_content: lambda{|hash| hash[:md_content]},
              }

              described = RealizationDescribed.new(bind: binding)
              self_described = described.get_described
              return self_described if show_how

              res = Satcom::Signal::Http.get(url: args[:article_link])
              return res unless res[0]

              doc = Nokogiri::HTML.parse(res[1][:body])
              original_contents = []
              doc.xpath("//*[@class='article__bd__detail']")[0].children.each do |children|
                original_contents << analysis_element(element: children)
              end

              items = {
                '：' => ':',
                '（' => '(',
                '）' => ')',
                '，' => ',',
                '。' => '.',
                '“' => ' *',
                '”' => '* ',
                '？' => '?',
              }
              reg = Regexp.new("[#{items.keys.join('')}]")
              fomat_contents = []
              original_contents.each do |content|
                str = case content
                when Array
                  content.flatten.join(' ')
                when String
                  content
                end
                fomat_contents << str.gsub(reg, items)
              end
              
              [true, {md_content: fomat_contents.join(args[:linefeed][:markdown])}]
            end

          def analysis_element(element:)
            if element.children.present?
              tmp = []
              element.children.each do |children|
                tmp << analysis_element(element: children)
              end
              return tmp
            end

            case element.name
            when 'br'
              return ''
            when 'comment'
              return element.text
            when 'text'
              return element.text
            when 'p', 'h4', 'b'
              tmp = []
              element.children.each do |children|
                tmp << analysis_element(element: children)
              end
              return tmp
            when 'img'
              url = element.attributes['src'].value
              return "![](#{url})"
            else 
              return "[error]未实现解析内容: #{element}"
            end
          end

        end 
      end
    end
  end
end