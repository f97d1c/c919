module Satcom
  module BusinessLogic
    module Finance
      module Fund
        module KeywordSearch
          extend self

          def blueprint
            method_name = '关键词搜索'
            explain = '根据给出的关键词,返回相关的基金以及基本信息.'
            realizations = [:realization_eastmoney]

            # 规定标准返回数据格式
            data_structure = {
              code: '代码',
              name: '名称',
              type: '类型',
            }

            described = BlueprintDescribed.new(bind: binding)
            described.set_attribute(key: 'keyword', name: '关键词', explain: nil, example: '5G', value_type: :string)
            
            described.get_described
          end 

          private
            def realization_eastmoney(show_how: false, keyword: 'keyword', page: 1, per: 20, **args)
              supplier_name = '天天基金'
              source_url = 'https://fundsuggest.eastmoney.com/FundSearch/api/FundSearchAPI.ashx'
              source_data_handel_method = 'Json键值对读取'

              # 根据主方法定义数据格式加工数据
              data_structure = {
                code: lambda{|hash| hash['code']},
                name: lambda{|hash| hash['name']},
                type: lambda{|hash| hash['categorydesc']},
              }

              described = RealizationDescribed.new(bind: binding)
              self_described = described.get_described
              return self_described if show_how

              params = {
                callback: nil,
                m: 10,
                t: 700,
                IsNeedBaseInfo: 0,
                IsNeedZTInfo: 0,
                key: keyword,
              }
              

              res = Satcom::Signal::Http.get(url: source_url, params: params)
              return res unless res[0]
              hash = JSON.parse(res[1][:body])

              tmp = []
              hash['Datas'].each do |data|
                tmp << data.slice('CODE', 'NAME', 'CATEGORYDESC').transform_keys{|key| key.to_s.downcase }
              end 

              [true, tmp] 
            end

        end 
      end
    end
  end
end