module Satcom
  module BusinessLogic
    module Finance
      module Fund
        module MarketOverviewQuantity
          extend self

          def blueprint
            method_name = '市场概况-基金数量(只)'
            explain = '返回市场中各类型基金数量'
            realizations = [:realization_eastmoney]

            data_structure = {
              whole: '全部',
              stock_type: '股票型',
              mixed_type: '混合型',
              bond_type: '债券型',
              exponential_type: '指数型',
              qdii: 'QDII',
              principal_guaranteed: '保本型',
              currency_type: '货币型',
            }
            
            described = BlueprintDescribed.new(bind: binding)
            described.get_described
          end 

          def realization_eastmoney(show_how: false, **args)
            supplier_name = '天天基金'
            source_url = "http://fund.eastmoney.com/Company/default.html"
            source_data_handel_method='Html解析'

            data_structure = {
              whole: lambda{|hash| hash['全部']},
              stock_type: lambda{|hash| hash['股票型']},
              mixed_type: lambda{|hash| hash['混合型']},
              bond_type: lambda{|hash| hash['债券型']},
              exponential_type: lambda{|hash| hash['指数型']},
              qdii: lambda{|hash| hash['QDII']},
              principal_guaranteed: lambda{|hash| hash['保本型']},
              currency_type: lambda{|hash| hash['货币型']},
            }

            described = RealizationDescribed.new(bind: binding)
            self_described = described.get_described
            return self_described if show_how

            res = Satcom::Signal::Http.get(url: source_url)
            return res unless res[0]

            doc = Nokogiri::HTML.parse(res[1][:body])
            datas = {}
            div_contents = doc.xpath("//*[@class='third-block jcommon-block']")
            
            tbody_contents = div_contents.css('tbody')

            title_contents = tbody_contents.css('tr')[0]
            titles = title_contents.css('th').map(&:text)[1..-1]

            data_contents = tbody_contents.css('tr')[2]
            datas = data_contents.css('td').map(&:text)[1..-1]
            
            tmp = {}
            titles.each_with_index do |title, index|
              tmp.merge!({title => datas[index].gsub(' ', '')})
            end 
            
            [true, tmp]
          end 

        end 
      end
    end
  end
end