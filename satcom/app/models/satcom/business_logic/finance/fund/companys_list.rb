module Satcom
  module BusinessLogic
    module Finance
      module Fund
        module CompanysList
          extend self

          def blueprint
            method_name = '公司基本信息'
            explain = '返回基金公司基本信息'
            realizations = [:realization_eastmoney]

            # 规定标准返回数据格式
            data_structure = {
              fund_company: '基金公司',
              time_of_establishment: '成立时间',
              total_management_scale: '全部管理规模(亿元) 截止日期',
              total_funds: '全部基金数',
              total_managers: '全部经理数',
              eastmoney_url: '天天基金详情链接',
            }

            described = BlueprintDescribed.new(bind: binding)   
            described.get_described
          end 

          private
            def realization_eastmoney(show_how: false, keyword: 'keyword', page: 1, per: 20, **args)
              supplier_name = '天天基金'
              source_url = 'http://fund.eastmoney.com/Company/default.html'
              source_data_handel_method = 'Html解析'

              # 根据主方法定义数据格式加工数据
              data_structure = {
                fund_company: lambda{|hash| hash[:fund_company]},
                time_of_establishment: lambda{|hash| hash[:time_of_establishment]},
                total_management_scale: lambda{|hash| hash[:total_management_scale]},
                total_funds: lambda{|hash| hash[:total_funds]},
                total_managers: lambda{|hash| hash[:total_managers]},
                eastmoney_url: lambda{|hash| hash[:eastmoney_url]},
              }

              described = RealizationDescribed.new(bind: binding)
              self_described = described.get_described
              return self_described if show_how

              res = Satcom::Signal::Http.get(url: source_url)
              return res unless res[0]
              doc = Nokogiri::HTML.parse(res[1][:body])

              datas = []
              doc.xpath("//*[@id='gspmTbl']").css('tbody').css('tr').each do |tr|
                td_contents = tr.css('td').map(&:text)
                array = td_contents[1..-1].delete_if{|str| str =~ /公司吧/}
                hash = {
                  fund_company: array[0],
                  time_of_establishment: array[1],
                  eastmoney_url: 'http://fund.eastmoney.com'+tr.css('a')[0].attributes['href'].value,
                  total_management_scale: array[3].gsub(/ |\r|\n/, ''),
                  total_funds: array[4],
                  total_managers: array[5],
                }
                datas << hash
              end 

              [true, datas] 
            end

        end 
      end
    end
  end
end