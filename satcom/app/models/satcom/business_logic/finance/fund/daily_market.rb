module Satcom
  module BusinessLogic
    module Finance
      module Fund
        module DailyMarket
          extend self

          def blueprint
            method_name = '每日行情'
            explain = '根据给出的基金代码以及相关参数,返回:净值日期,单位净值,累计净值,日增长率,申购状态,赎回状态,分红送配等属性每日状态.'
            realizations = [:realization_eastmoney]

            # 规定标准返回数据格式
            data_structure = {
              fund_code: '基金代码',
              net_value_date: '净值日期',
              unit_net_value: '单位净值',
              accumulated_net_value: '累计净值',
              daily_growth_rate: '日增长率',
              subscription_status: '申购状态',
              redemption_status: '赎回状态',
              dividend_distribution: '分红送配',
            }

            described = BlueprintDescribed.new(bind: binding)
            described.set_attribute(key: 'code', name: '基金代码', explain: '', example: '377240', value_type: :string)
            described.set_attribute(key: 'per', name: '每页条数', explain: '每页返回数据条数,以20的倍数为传值标准,小数将向下取整', example: nil, default: 20, value_type: :integer, necessary: false)
            described.set_attribute(key: 'current_page', name: '当前页数', explain: '配合每页条数获取目标范围数据,小数将向下取整', example: nil, default: 1, value_type: :integer, necessary: false)

            described.get_described
          end 

          private
            def realization_eastmoney(show_how: false, code: nil, current_page: 1, per: 20, **args)
              supplier_name = '天天基金'
              source_url = 'http://fund.eastmoney.com/f10/F10DataApi.aspx'
              source_data_handel_method = 'Js变量解析'
              # 根据主方法定义数据格式加工数据
              @code = code
              data_structure = {
                fund_code: lambda{|hash| @code },
                net_value_date: lambda{|hash| hash['净值日期']},
                unit_net_value: lambda{|hash| hash['单位净值']},
                accumulated_net_value: lambda{|hash| hash['累计净值']},
                daily_growth_rate: lambda{|hash| hash['日增长率']},
                subscription_status: lambda{|hash| hash['申购状态']},
                redemption_status: lambda{|hash| hash['赎回状态']},
                dividend_distribution: lambda{|hash| hash['分红送配']},
              }

              described = RealizationDescribed.new(bind: binding)
              self_described = described.get_described
              return self_described if show_how
              
              page_range = (((current_page-1)*per/20)..(current_page*per/20-1))
              page_range = (1..1) if page_range.to_a == [0]

              tmp = []
              page_range.each do |page|
                params = {
                  type: 'lsjz',
                  code: code,
                  page: page,
                  # 天天基金最多每次20条
                  per: 20
                }

                res = get_eastmoney_response(source_url: source_url, params: params)
                return res unless res[0]
                tmp << res[1]
              end

              [true, tmp.flatten]
            end

            def get_eastmoney_response(source_url:, params:)
              res = Satcom::Signal::Http.get(url: source_url, params: params)
              return res unless res[0]
              body = res[1][:body]
              
              remove_var = body.gsub(/(var\s[a-zA-Z]{1,}\=)(.*)(;$)/, '\2')
              html_content = remove_var.gsub(/(\{\scontent:\")(.*)(\",records.*$)/, '\2')
              html_object = Nokogiri::HTML.parse(html_content)
              datas = [html_object.css('th').map{|children| children.text}]

              datas = datas + html_object.css('tr').map do |children|
                children.css('td').map do |td|
                  td.text
                end 
              end 

              datas.delete_if{|array| !array.present?}

              tmp = []
              datas[1..-1].each do |array|
                hash = {}
                array.each_with_index do |item, index|
                  hash.merge!({datas[0][index] => item})
                end 
                tmp << hash
              end 

              [true, tmp]
            end 

        end 
      end
    end
  end
end