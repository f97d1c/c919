# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_06_27_074536) do

  create_table "dashboard_other_colors", force: :cascade do |t|
    t.string "name", null: false
    t.string "hex", null: false
    t.string "rgb", null: false
    t.string "lab", null: false
    t.string "lib", null: false
    t.string "form", null: false
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["deleted_at"], name: "index_dashboard_other_colors_on_deleted_at"
    t.index ["form"], name: "index_dashboard_other_colors_on_form"
    t.index ["hex"], name: "index_dashboard_other_colors_on_hex"
    t.index ["lab"], name: "index_dashboard_other_colors_on_lab"
    t.index ["lib"], name: "index_dashboard_other_colors_on_lib"
    t.index ["name"], name: "index_dashboard_other_colors_on_name"
    t.index ["rgb"], name: "index_dashboard_other_colors_on_rgb"
  end

end
