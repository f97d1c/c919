module Dashboard
  class RestfulController < ApplicationController
    
    after_action :about_cors, only: [:request_api]

    def example
      @exposed_api = Dashboard::Restful::Public::exposed_api
    end

    def exposed_api
      exposed_api = {}
      Dashboard::Restful::Public::exposed_api.each do |key, info|
        exposed_api.merge!({key => {name: info[:method_name], explain: info[:explain]}})
      end

      render json:{
        dashboard: {
          name: '图形显示接口',
          explain: '提供各种图表,列表在内的图形显示服务.',
          exposed_api: exposed_api
        }
      }
    end

    def request_api_params
      api_code = params[:api_code]
      return (render json: {success: false, result: '接口标识(api_code)不能为空'}) unless api_code
      exposed_api = Restful::Public::exposed_api
      current_api = exposed_api[api_code.to_sym]
      render json: {success: true, request_params: current_api[:attributes], explain: current_api[:explain]}
    end

    def request_api
      exposed_api = Restful::Public::exposed_api
      current_api = exposed_api[(params[:api_code] || '').to_sym]

      # TODO 初步了解为rails5的安全机制 去除存在风险 暂时先这样
      args = params.permit!.to_hash.reject!{|key| ["response_type","api_code", "action", "controller"].include?(key)}
      
      if current_api
        res = begin
          current_api[:method_path].call(args.deep_symbolize_keys)
        rescue
          info = {errors: {message: $!.to_s, path: $@}}
          print info
          return [false, 'DashBoard运行时异常', info]
        end
      end

      unless current_api
        res = if params[:response_type] == 'json'
          [false, '根据api_code未找到相关授权接口, 允许调用接口及相关介绍参考explain_api内容.', {explain_api: Dashboard::BusinessLogic::Public::explain_api}]
        else
          [false, "根据api_code未找到相关授权接口, 允许调用接口及相关介绍参考:#{Dashboard::BusinessLogic::Public::explain_api.to_a.map{|array| array.join(':')}.join(',')}"]
        end 
      end

      if res[0]
        html_content = render_to_string(:partial => current_api[:template], :locals => res[1][:generated_data])
      end 

      case params[:response_type]
      when 'json', 'html'
        send("request_api_response_#{params[:response_type]}", {res: res, result: (html_content || nil)})
      else
        render html: '未知response_type' 
      end
    end 

    def test

    end 

    private

      def request_api_response_json(res:, result: nil)
        hash = {success: res[0]}.merge!(res[1].is_a?(Hash) ? res[1] : {result: res[1]})
        hash.merge!({result: result}) if result
        render json: hash.merge!(res[2] || {})
      end

      def request_api_response_html(res:, result: nil)
        return (render html: res[1]) unless res[0]
        render html: (result || res[1])
      end

      def about_cors
        response.set_header('Access-Control-Allow-Origin', '*')
        response.set_header('Access-Control-Allow-Methods', 'POST, PUT, DELETE, GET, OPTIONS')
        response.set_header('Access-Control-Request-Method', '*')
        response.set_header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization')
      end 

  end
end