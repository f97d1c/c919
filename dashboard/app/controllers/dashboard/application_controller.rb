module Dashboard
  class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception
    # around_action :at_around_action
    skip_before_action :verify_authenticity_token

    private 
      def at_around_action
        begin
          result = yield
          respond_to do |format|
            format.html 
            format.xml  { render xml: result.to_xml }
            format.json { render json: result.to_json }
          end
        rescue => exception
          reg = Regexp.new("#{controller_name}|#{action_name}")
          raise "错误说明: #{$!} \n发生位置: #{$@.select{|str| str =~ reg}.join("\n")}"
        end
      end 

  end
end
