module Dashboard
  module Restful
    # 所有对外提供服务功能应该都由该module负责
    module Public
      extend self

      def exposed_api
        tmp = {}
        BusinessLogic::Public::exposed_api.each do |key, value|
          extra_params = [
            {key: 'api_code', name: '接口标识', explain: '访问该接口的对应唯一标识', regular: /^[^\s]+$/, default: nil, example: key, necessary: true, value_type: 'string'},
            {key: 'response_type', name: '响应类型', explain: '根据请求内容返回html/json类型响应内容', regular: /^[^\s]+$/, default: nil, example: 'html', necessary: true, value_type: 'string'},
          ]

          value[:attributes] = (value[:attributes] + extra_params )
          tmp.merge!({key => value})
        end
        tmp
      end 

    end
  end
end