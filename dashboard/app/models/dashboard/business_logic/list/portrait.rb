module Dashboard
  module BusinessLogic
    module List

      # 纵向表格
      module Portrait
        extend self

        # 所有可调用接口集合
        def exposed_api
          {
            base_table: base_table(show_how: true),
          }
        end
        
        def base_table(show_how: false, **args)
          method_name = '纵向基础表格'
          explain = '根据提供的Json数据返回对应的Bootstrap样式的纵向基础表格'
          rely_assets = ["bootstrap_4", "jQuery_3_5_1"]
          described = SelfDescribed.new(bind: binding)
          described.set_attribute(key: 'base_params', name: '表格基础数据', explain: '包含在数组内的Json格式键值对', example: :public_file, value_type: :key_value)
          described.set_attribute(key: 'split_size', name: '每行数据个数', explain: nil, example: nil, default: 2, value_type: :integer)
          described.set_attribute(key: 'table_class', name: '表格样式', explain: '通过传递官方提供的样式名称自定义表格样式', example: nil, default: ["table-dark", "table-hover", "table-bordered"], value_type: :array)

          self_described = described.get_described
          return self_described if show_how

          res = Inspect::inspect_all(self_described: self_described, reality: args)
          return res unless res[0]
          generated_data = args
          [true, {generated_data: generated_data}]
        end
      end

    end
  end
end