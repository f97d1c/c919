module Dashboard
  module BusinessLogic
    module List

      # 水平方向列表
      module Horizontal
        extend self

        # 所有可调用接口集合
        def exposed_api
          {
            base_table: base_table(show_how: true),
            column_parsed: column_parsed(show_how: true),
          }
        end
        
        def base_table(show_how: false, **args)
          method_name = '横向基础表格'
          explain = '根据提供的Json数据返回对应的Bootstrap样式的横向基础表格'
          rely_assets = ["bootstrap_4", "jQuery_3_5_1"]
          described = SelfDescribed.new(bind: binding)
          described.set_attribute(key: 'base_params', name: '表格基础数据', explain: '包含在数组内的Json格式键值对', example: :public_file, value_type: :array)
          described.set_attribute(key: 'table_class', name: '表格样式', explain: '通过传递官方提供的样式名称自定义表格样式', example: nil, default: ["table-dark", "table-hover", "table-bordered"], value_type: :array)

          self_described = described.get_described
          return self_described if show_how

          res = Inspect::inspect_all(self_described: self_described, reality: args)
          return res unless res[0]
          generated_data = args
          
          generated_data.merge!({
            column_names: args[:base_params].first.keys, 
            column_datas: args[:base_params].map(&:values)
          })
          
          [true, {generated_data: generated_data}]
        end

        def column_parsed(show_how: false, **args)
          method_name = '数据列分析表格'
          explain = '将HTML表格用作图表的数据源,通过引用页面中现有的HTML数据表来构建图表.'
          rely_assets = ["highcharts"]
          described = SelfDescribed.new(bind: binding)
          described.set_attribute(key: 'table_datas', name: '表格基础数据', explain: '包含在数组内的Json格式键值对', example: :public_file, value_type: :key_value)
          described.set_attribute(key: 'title', name: '标题', explain: '图表的标题', example: :public_file, value_type: :string, necessary: false)
          described.set_attribute(key: 'subtitle', name: '副标题', explain: '图表的副标题', example: :public_file, value_type: :string, necessary: false)
          described.set_attribute(key: 'describe', name: '描述', explain: '关于时间线的相关说明,描述内容.', example: :public_file, value_type: :string, necessary: false)
          described.set_attribute(key: 'y_unit_name', name: '单位名称', explain: 'Y轴数字计量单位名称.', example: '百分比(%)', necessary: false, value_type: :string)

          self_described = described.get_described
          return self_described if show_how          

          res = Inspect::inspect_all(self_described: self_described, reality: args)
          return res unless res[0]
          generated_data = args
          
          [true, {generated_data: generated_data}]
        end 

      end

    end
  end
end