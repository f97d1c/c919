module Dashboard
  module BusinessLogic
    # 自描述类 用于对接口进行定义和描述                                                 
    class SelfDescribed
      
      def initialize(bind: )
        @bind = bind
        @self_ = eval("self", @bind)
        @method = eval("__method__", @bind)
        @attributes = []
      end

      def columns
        {
          method_name: '用于定义方法的中文名称, 起到简单解释的作用',
          explain: '关于方法的进一步详细阐述',
          method_path: '方法调用路径',
          template: '方法所需调用到的页面模板',
          attributes: '方法所用的属性,即所需参数',
        }
      end 

      def get_described
        # raise "获取自描述内容前先定义属性值(set_attribute)" unless @attributes.present?
        tmp = {}
        columns.keys.each do |column|
          method_name = "get_#{column}".to_sym
          res = if respond_to?(method_name)
            send(method_name)
          else
            get_variable.call(column)
          end

          return res unless res[0]
          tmp.merge!(res[1])
        end 
        tmp
      end 

      def get_variable
        lambda do |variable|
          variable_ = (eval(variable.to_s, @bind) rescue nil)
          return [false, "宿主方法内未定义:#{variable}变量(#{columns[variable.to_sym] || '暂无任何描述信息'})"] unless variable_
          [true, {variable => variable_}.symbolize_keys]
        end 
      end 

      def get_method_path
        method_path = (eval("method_path", @bind) rescue nil)
        return [true, {method_path: method_path}] if method_path
        method_path = lambda{|**args| @self_.send(@method, args)}
        [true, {method_path: method_path}]
      end 

      def get_attributes
        rely_assets = (eval("rely_assets", @bind) rescue nil)
        raise "宿主方法内(#{@method})未定义:rely_assets变量(定义依赖资源)" unless rely_assets
        # 加载通用属性
        currency_attributes(rely_assets: rely_assets)
        [true, {attributes: @attributes}]
      end 

      def get_template
        res = get_variable.call(:template)
        return res if res[0]

        template = 'shared/' + @self_.to_s.gsub(/\:\:/, '/').underscore.gsub(/business_logic\//, '') +"/#{@method.to_s}"
        [true, {template: template}]
      end 

      def set_attribute(key:, name:, explain:, regular: nil, default: '', example:, necessary: true, value_type:)

        example_ = case example
        when :public_file, "public_file"
          file_path = 'public/'+@self_.to_s.gsub(/\:\:/, '/').underscore.gsub(/business_logic/, 'example_attributes')+"/#{@method.to_s}/#{key}"

          if File::file?(file_path+'.json')
            file = File.open(file_path+'.json')
            content = JSON.parse(file.read)
            file.close
            content.to_json
          elsif File::file?(file_path+'.txt')
            file = File.open(file_path+'.txt')
            content = file.read
            file.close
            content
          else
            raise "系统异常: 示例数据文件不存在或格式不支持, file_path: #{file_path}"
          end 
        else
          example
        end 

        attribute = {
          key: key,
          name: name,
          explain: explain,
          regular: regular,
          default: default,
          example: example_,
          necessary: necessary,
          value_type: value_type
        }
        @attributes << attribute
      end 

      private

        # 通用属性
        def currency_attributes(rely_assets:)
          set_attribute(key: 'unique_codes', name: '唯一标识', explain: '将用于关于请求中所有唯一性标识,包括但不限于请求中图表的id标识(数组中下标为0元素),后续扩展将补充描述内容', default: [SecureRandom.uuid], example: nil, necessary: false, value_type: :array)

          set_attribute(key: 'rely_assets', name: '依赖资源', explain: '该接口依赖Css或Js资源,根据宿主页面是否包含示例值内资源进行选择,默认不返回依赖外部资源.', default: [], example: rely_assets, necessary: false, value_type: :array)
          
          set_attribute(key: 'response_asset_type', name: '响应资源类型', explain: '当选择传递依赖资源时,可通过返回资源链接标签(link_label)或原始资源内容(original_content)', default: 'link_label', example: nil, necessary: false, value_type: :string)
        end

    end

  end 
end