module Dashboard
  module BusinessLogic
    module Chart
      
      module Finance
        extend self
      
        # 所有可调用接口集合
        def exposed_api
          {
            candlestick: candlestick(show_how: true),
          }
        end

        def candlestick(show_how: false, **args)
          method_name = '蜡烛图'
          explain = '用于生成包含开盘价,最高价,最低价以及收盘价在内的蜡烛图.'
          rely_assets = ["amcharts_4"]

          described = SelfDescribed.new(bind: binding)
          described.set_attribute(key: 'base_datas', name: '基础数据', explain: '每个数据单元由四部分组成:时间(date),开盘价(open),最高价(high),最低价(low),收盘价(close)', example: :public_file, value_type: :array)
          described.set_attribute(key: 'trend_color', name: '趋势颜色', explain: '["开盘价高于收盘价颜色", "开盘价低于收盘价颜色"]', example: nil, value_type: :array, default: ["red", "green"], necessary: false)
          self_described = described.get_described
          return self_described if show_how

          res = Inspect::inspect_all(self_described: self_described, reality: args)
          return res unless res[0]
          generated_data = args
          
          [true, {generated_data: generated_data}]
        end

      end
    end
  end
end