module Dashboard
  module BusinessLogic
    module Chart
      
      module Tree
        extend self
      
        # 所有可调用接口集合
        def exposed_api
          {
            word_tree: word_tree(show_how: true),
            orgchart: orgchart(show_how: true),
          }
        end

        def word_tree(show_how: false, **args)
          method_name = '词树'
          explain = '根据提供的关键词组织成树结构'
          rely_assets = ["google_charts"]
          described = SelfDescribed.new(bind: binding)
          described.set_attribute(key: 'word_datas', name: '基础数据', explain: '将存在层级结构的词组按空格分隔,以每个最小独立单元组成数组', example: :public_file, value_type: :array)
          described.set_attribute(key: 'root_word', name: '根词组', explain: '将以此词组为顶点进行组织', example: 'A股', value_type: :string)
          self_described = described.get_described
          return self_described if show_how

          res = Inspect::inspect_all(self_described: self_described, reality: args)
          return res unless res[0]
          generated_data = args
          
          [true, {generated_data: generated_data}]
        end


        def orgchart(show_how: false, **args)

          method_name = '组织结构图'
          explain = '组织图是节点层次结构的图,通常用于描述组织中的上级/下级关系.'
          rely_assets = ["google_charts"]
          described = SelfDescribed.new(bind: binding)
          described.set_attribute(key: 'tree_datas', name: '基础数据', explain: '每个数组由三部分组成[{"v":"子节点引用标识", "f":"显示名称"},"父节点标识", "说明"]或["显示名称","父节点标识", "说明"]', example: :public_file, value_type: :array)
          self_described = described.get_described
          return self_described if show_how

          res = Inspect::inspect_all(self_described: self_described, reality: args)
          return res unless res[0]
          generated_data = args
          
          [true, {generated_data: generated_data}]
        end

      end
    end
  end
end