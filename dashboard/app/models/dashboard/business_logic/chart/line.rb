module Dashboard
  module BusinessLogic
    module Chart
      
      module Line
        extend self
      
        # 所有可调用接口集合
        def exposed_api
          {
            broken_line: broken_line(show_how: true),
            time_line: time_line(show_how: true),
            synchronized_charts: synchronized_charts(show_how: true),
            live_data: live_data(show_how: true),
            broken_line_scalable_timeline: broken_line_scalable_timeline(show_how: true),
          }
        end

        def broken_line(show_how: false, **args)
          method_name = '折线图'
          explain = '根据提供的Json数据返回对应的折线图图表'
          rely_assets = ["chart_js"]
          described = SelfDescribed.new(bind: binding)
          described.set_attribute(key: 'line_datas', name: '数据项集合', explain: 'Json键值对中,外层键代表折线的名称,值为折线节点值组成的数组.', example: :public_file, value_type: :key_value)
          described.set_attribute(key: 'object_colors', name: '数据项颜色', explain: 'Json数组', example: ['red'], value_type: :array)
          described.set_attribute(key: 'title', name: '表格标题', explain: '', example: :public_file, value_type: :string, necessary: false)
          self_described = described.get_described
          return self_described if show_how
          
          res = Inspect::inspect_all(self_described: self_described, reality: args)
          return res unless res[0]
          generated_data = args
          x_sorts = generated_data[:line_datas].first[1].keys

          tmp = []
          generated_data[:line_datas].each_with_index do |hash, index|
            name, datas = hash[0], hash[1]
            x_sorts_ = datas.keys
            return [false, "键值对数据不匹配,首位:#{x_sorts.to_json}, 当前:#{x_sorts_.to_json}"] unless x_sorts == x_sorts_

            color = generated_data[:object_colors][index]
            tmp << {
              label: name,
              data: datas.values,
              backgroundColor: color, 
              borderColor: color, 
              fill: false
            }
          end 

          generated_data.merge!({line_datas: tmp, x_sorts: x_sorts})
          
          [true, {generated_data: generated_data}]
        end
        
        def time_line(show_how: false, **args)
          method_name = '时间线图'
          explain = '时间线图表用于在时间轴上放置事件.'
          rely_assets = ["highcharts"]
          
          described = SelfDescribed.new(bind: binding)
          described.set_attribute(key: 'line_datas', name: '数据项集合', explain: '数组中按照时间顺序进行排列', example: :public_file, value_type: :array)
          described.set_attribute(key: 'title', name: '标题', explain: '时间线的标题', example: :public_file, value_type: :string, necessary: false)
          described.set_attribute(key: 'subtitle', name: '副标题', explain: '时间线的副标题', example: :public_file, value_type: :string, necessary: false)
          described.set_attribute(key: 'described', name: '描述', explain: '关于时间线的相关说明,描述内容.', example: :public_file, value_type: :string, necessary: false)

          self_described = described.get_described
          return self_described if show_how
          
          res = Inspect::inspect_all(self_described: self_described, reality: args)
          return res unless res[0]
          generated_data = args
          
          [true, {generated_data: generated_data}]
        end

        def synchronized_charts(show_how: false, **args)
          method_name = '同步图'
          explain = '随着鼠标在x轴的移动,页面上所有折线图都会跟随移动.'
          rely_assets = ["highcharts"]
          described = SelfDescribed.new(bind: binding)
          described.set_attribute(key: 'line_datas', name: '数据项集合', explain: '数组中按照相同顺序进行排列', example: :public_file, value_type: :key_value)
          described.set_attribute(key: 'describe', name: '描述', explain: '关于图表的相关说明,描述内容.', example: 'This demo shows how related charts can be synchronized. Hover over one chart to see the effect in the other charts as well.This is a technique that is commonly seen in dashboards, where multiple charts are often used to visualize related information.', value_type: :string, necessary: false)
          self_described = described.get_described
          return self_described if show_how
          
          res = Inspect::inspect_all(self_described: self_described, reality: args)
          return res unless res[0]
          generated_data = args
          
          [true, {generated_data: generated_data}]
        end
        
        def live_data(show_how: false, **args)
          method_name = '实时图'
          explain = '提供动态数据文件地址,实时刷新数据并展示'
          rely_assets = ["highcharts"]
          described = SelfDescribed.new(bind: binding)
          described.set_attribute(key: 'data_source_url', name: '数据源地址', explain: '用于获取实时数据', example: 'https://demo-live-data.highcharts.com/time-data.csv', value_type: :string)
          described.set_attribute(key: 'polling_time', name: '轮询时间', explain: '刷新数据间隔时间,单位:秒', example: nil, value_type: :string, default: 1, necessary: false)
          self_described = described.get_described
          return self_described if show_how
          
          res = Inspect::inspect_all(self_described: self_described, reality: args)
          return res unless res[0]
          generated_data = args
          polling_time = generated_data[:polling_time]
          generated_data[:polling_time] = ((polling_time.to_i >= 1 ) ? polling_time.to_i : 1)
          
          [true, {generated_data: generated_data}]
        end

        def broken_line_scalable_timeline(show_how: false, **args)
          method_name = '折线图(可伸缩时间轴)'
          explain = '根据提供的Json数据返回对应的折线图图表'
          rely_assets = ["chart_js"]
          described = SelfDescribed.new(bind: binding)
          described.set_attribute(key: 'line_datas', name: '折线数据', explain: '时间为键,数值为值的Json键值对', example: :public_file, value_type: :key_value)
          self_described = described.get_described
          return self_described if show_how
          
          res = Inspect::inspect_all(self_described: self_described, reality: args)
          return res unless res[0]
          generated_data = args

          datas = []
          generated_data[:line_datas].each do |key, value|
            array = key.to_s.gsub(/[\-| |\:]/, ',').split(',')
            date = Time.new(*array)
            datas << {date:date, value: value}
          end 
           

          generated_data.merge!({line_datas: datas})
          
          [true, {generated_data: generated_data}]
        end

      end
    end
  end
end