// 网站底部通用信息写入

function setSiteAddress(parent){
  let address = document.createElement('address')
  address.setAttribute('id', 'address-info')
  address.setAttribute('class', 'text-center')
  let a = document.createElement('a')
  a.setAttribute('href', 'mailto:ff4c00@gmail.com')
  a.innerHTML = '联系邮箱'
  address.appendChild(a)
  parent.appendChild(address)
}

function setSiteIcp(parent){
  let p = document.createElement('p')
  p.setAttribute('id', 'icp-mark')
  p.setAttribute('class', 'text-center')
  p.innerHTML = '京ICP备2021000395'
  parent.appendChild(p)
}

document.addEventListener('DOMContentLoaded', function() {
  parent = (document.getElementById('space') || document.getElementsByClassName('container')[0] || document.getElementsByClassName('main_title')[0] || document.body)
  var div = document.createElement('div')
  div.setAttribute('id', 'footer')

  setSiteAddress(div)
  setSiteIcp(div)
  parent.appendChild(div)
}, false);

// 百度统计
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?836466303974b84a50b13ff73803e1f1";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();