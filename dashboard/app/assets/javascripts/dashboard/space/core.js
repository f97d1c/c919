space = document.getElementById('space') || new Object()
// import { element } from './element.js';
// space.element = element();

// 网络相关
space.network = Object.create(space)
// 页面相关
space.page = Object.create(space)
// 数据相关
space.data = Object.create(space)
// 其他
space.other = Object.create(space)
// 字符串相关
space.string = Object.create(space)
// 帮助方法
space.helper = Object.create(space)



// ========================== 网络相关 ==========================
space.network.ajax = function (request, callBack) {
  defaultValues = {"type" : "Get", "url" : null, "params" : {}}
  request = $.extend({}, defaultValues, request)
  if (!request.url) return [false, 'url不能为空']
  
  $.ajax({
    type: request.type,
    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
    url: request.url,
    data: request.params,
    success: function(result) {
      if (typeof(callBack) == 'function') callBack([true, result])
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      if (errorThrown.length > 0) space.page.alert(errorThrown)
      if (typeof(callBack) == 'function') callBack([false, errorThrown, XMLHttpRequest])
    }
  })
}

// ========================== 页面相关 ==========================
space.page.alert = function (message) {
  alert(message)
}

// 监听是否到达页面底部
space.page.listening = false
space.page.listeningBottom = async function (callBack) {
  if (space.page.listening) return

  space.page.listening = true
  scrollTop = space.scrollTop() // 滚动条距离顶部的高度
  scrollHeight = space[0].scrollHeight // 当前div的总高度
  clientHeight = space.height() // 当前可视页面高度

  // 距离顶部+当前高度 >= 文档总高度 则滑动到底部
  if (!(scrollTop + clientHeight >= scrollHeight - 200)) {
    space.page.listening = false
    return
  }
  
  if (typeof(callBack) == 'function') callBack()
}

// ========================== 数据相关 ==========================
// base64编码内容生成blob
space.data.base64ToBlob = function(base64, mimetype, slicesize) {
  if (!window.atob || !window.Uint8Array) return null;
  mimetype = mimetype || '';
  slicesize = slicesize || 512;
  var bytechars = atob(base64);
  var bytearrays = [];
  for (var offset = 0; offset < bytechars.length; offset += slicesize) {
    var slice = bytechars.slice(offset, offset + slicesize);
    var bytenums = new Array(slice.length);
    for (var i = 0; i < slice.length; i++) {
      bytenums[i] = slice.charCodeAt(i);
    }
    var bytearray = new Uint8Array(bytenums);
    bytearrays[bytearrays.length] = bytearray;
  }
  return new Blob(bytearrays, {type: mimetype});
};

// 根据内容生成文件
space.data.downloadFile = function(params){
  defaultValues = {fileName: null, base64: null, mimetype: null}
  params = $.extend({}, defaultValues, params)
  if (!params.fileName) return [false, 'fileName(文件名)不能为空'];
  if (!params.base64) return [false, 'base64(base64编码文件内容)不能为空'];
  if (!params.mimetype) return [false, 'mimetype(文件格式)不能为空'];

  var blob = space.data.base64ToBlob(params.base64, params.mimetype);
  var downloadUrl = window.URL.createObjectURL(blob);
  var anchor = document.createElement("a");
  anchor.href = downloadUrl;
  anchor.download = params.fileName;
  anchor.click();
  window.URL.revokeObjectURL(blob);
  return [true, '']
}

// ========================== 其他 ==========================
// 休眠
space.other.sleep = function (ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

// ========================== 字符串相关 ==========================

// 判断字符串是否为JSON格式
space.string.isJSON = function (item) {
  item = typeof item !== "string" ? JSON.stringify(item) : item;

  try {
    item = JSON.parse(item);
  } catch (e) {
    return false;
  }

  if (typeof item === "object" && item !== null) {
    return true;
  }

  return false;
}

// ========================== 帮助方法 ==========================

space.helper.loadAssets = async function(rely_assets, judgeLoadedCallBack, callBack){
  let body = {
    api_code: 'publish_other_request_assets',
    rely_assets: rely_assets,
    response_type: 'json',
  }

  let requestOptions = {
    method: 'POST',
    redirect: 'follow',
    mode: 'cors',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify(body),
  }

  await fetch("http://192.168.8.133:3111/dashboard/restful/request_api", requestOptions)
    .then(response => response.text())
    .then(result => {
      let resObj = JSON.parse(result)
      if (!resObj.success) return [false, (resObj.result || '返回内容为空'), resObj]

      let array_ = resObj.result.split(/\n/)
      let array = array_.filter(function (obj) { return (obj != ' ') })
      for (let item of array) {
        let attributes = item.split(/ |\>/)
        let tagName = attributes[0].split('<')[1]
        let target = document.createElement(tagName)
  
        let usefulAttributes = attributes.filter(function (str) { return (/href|rel|media|src/.test(str)) })
        for (let attributes of usefulAttributes) {
          let arr = attributes.split('=')
          target.setAttribute(arr[0], arr[1].replace(/\"/g, ''))
        }
        document.head.appendChild(target)
      }

      return [true, '']
    })
    .catch(error => console.log('error', error))

  if (!judgeLoadedCallBack()){
    for (let i = 0; i < 100; i++) {
      let sleepThreshold = 1
      await new Promise(resolve => setTimeout(resolve, sleepThreshold))
      console.log('waiting assets ready: ' + (i + 1) * sleepThreshold + 'ms')
      if (judgeLoadedCallBack()) break;
    }
  }

  if (!!callBack) return callBack()
}