class Template {
  request(params, callBack){
    let defaultValue ={
      url: undefined,
      method: "Get",
      redirect: 'follow',
      mode: 'cors',
      headers: {
        'content-type': 'application/json; charset=UTF-8',
      },
      params: JSON.stringify({})
    }

    params = Object.assign(defaultValue, params)
    if (!params.url) return [false, 'url不能为空']

    let requestOptions = {
      method: params.method,
      redirect: params.redirect,
      mode: params.mode,
      headers: params.headers,
      body: JSON.stringify(params.params),
    }

    fetch(params.url, requestOptions)
      .then(response => response.text())
      .then(result => {
        var tmp
        try {
          tmp = JSON.parse(result)
        } catch (e) {
          tmp = result
        }
        
        if (typeof(callBack) == 'function') callBack([true, tmp])
      })
      .catch(error => {
        if (typeof(callBack) == 'function') callBack([false, '接口请求失败: '+error])
      });
  }
}

class Main extends Template {
  dashboard (params, callBack){
    let defaultValue = {api_code: undefined, rely_assets: []}
    params = Object.assign(defaultValue, params)
    if (!params.api_code) return [false, 'api_code 不能为空'];
  
    let address = 'http://192.168.8.133:3111'
    let route = '/dashboard/restful/request_api'

    let params_ = {
      url: address+route,
      method: 'Post',
      params: params,
    }
    this.request(params_, callBack);
  };

  satcom (params, callBack){
    let defaultValue = {
      api_code: undefined, 
      // key_type: 'en'
    }
    params = Object.assign(defaultValue, params)
    if (!params.api_code) return [false, 'api_code 不能为空'];
  
    let address = 'http://192.168.8.133:3113'
    let route = '/satcom/restful/request_api'

    let params_ = {
      url: address+route,
      method: 'Post',
      params: params,
    }
    this.request(params_, callBack);
  }
}

wireway = (typeof wireway === 'undefined') ? new Main() : wireway
