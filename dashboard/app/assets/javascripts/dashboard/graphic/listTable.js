class ListTable {
  space = {}
  constructor(params) {
    let defaultValue = {
      columnNames: [], // 抬头显示内容
      columnKeys: undefined, // td内容对应对象键
      id: 'undefined',
      class: 'table table-hover table-striped table-bordered',
      objects: [],
      firstTr: null, // 标记当前第一个tr对象
      appendMode: 'insertBefore', // 追加模式 默认在最前面追加
      newRecord: false,
      emptyHistory: false, // 每次追加内容时是否清空历史内容
      needRowIndex: true, // 是否需要显示行号
      currentIndex: 0, // 当前行号
    }
    params = Object.assign(defaultValue, params)

    for (let [key, value] of Object.entries(params)) {
      this.space[key] = value
    }
    this.space.columnKeys ||= this.space.columnNames

    this.space.table = document.createElement('table')
    this.space.table.setAttribute('id', this.space.id)
    this.space.table.setAttribute('class', this.space.class)

    this.space.thead = document.createElement('thead')
    this.space.table.appendChild(this.space.thead)

    this.space.tbody = document.createElement('tbody')
    this.space.table.appendChild(this.space.tbody)
  }

  renderThead() {
    let tr = document.createElement('tr')
    if (this.space.needRowIndex){
      let th = document.createElement('th')
      th.setAttribute('class', 'text-center')
      th.innerHTML = '序号'
      tr.appendChild(th)
    }
    this.space.columnNames.forEach(name => {
      let th = document.createElement('th')
      th.setAttribute('class', 'text-center')
      th.innerHTML = name
      tr.appendChild(th)
    })
    this.space.thead.appendChild(tr)
  }

  isJson(item) {
    item = typeof item !== "string" ? JSON.stringify(item) : item;
  
    try {
      item = JSON.parse(item);
    } catch (e) {
      return false;
    }
  
    if (typeof item === "object" && item !== null) {
      return true;
    }
  
    return false;
  }

  renderObjects(params) {
    let defaultValue = {
      objects: this.space.objects,
      columnKeys: (this.space.columnKeys || this.space.columnNames),
      laterAdd: false,
      appendMode: this.space.appendMode,
      newRecord: this.space.newRecord,
      emptyHistory: this.space.emptyHistory,
    }
    
    params = Object.assign(defaultValue, params)
    if(params.emptyHistory){
      this.space.tbody.innerHTML = ''
      this.space.firstTr = null
      this.space.currentIndex = 0
    }

    params.objects.forEach(object => {
      let tr = document.createElement('tr')
      if (params.newRecord) tr.setAttribute('style', 'background-color: #c3e6cb78;')
      if (this.space.needRowIndex){
        let td = document.createElement('td')
        td.innerHTML = (this.space.currentIndex+=1)
        tr.appendChild(td)
      }
      params.columnKeys.forEach(key => {
        let td = document.createElement('td')

        if (this.isJson(object[key])){
          // TODO: 这里应该根据屏幕宽度 动态调整
          td.setAttribute('style', 'max-width: 200px;')
          let content = ((typeof object[key] == 'object') ? object[key] : JSON.parse(object[key]))
          // td.innerHTML = "<pre><code>"+JSON.stringify(content, undefined, 2)+"</code></pre>"
          td.innerHTML = "<pre style='font-size: 1rem; margin: 0 0;'><code>"+JSON.stringify(content)+"</code></pre>"
        }else{
          td.innerHTML = object[key]
        }
        tr.appendChild(td)
      })
      if (params.appendMode == 'insertBefore') {
        this.space.tbody.insertBefore(tr, this.space.firstTr)
        this.space.firstTr = tr
      } else {
        this.space.tbody.appendChild(tr)
      }
    })

    if (params.laterAdd) return [true, this.space.table.outerHTML]
  }

  init() {
    this.renderThead()
    this.renderObjects()
    return [true, this.space.table.outerHTML]
  }

}