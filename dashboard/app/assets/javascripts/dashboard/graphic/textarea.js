
class Textarea {
  renderToTextarea (object, params) {
    let defaultValue = {
      id: undefined,
      class: 'form-control',
      rows: 3,
    }

    params = Object.assign(defaultValue, params)
    if (!object) return [false, '渲染对象(object)不能为空']
    // <textarea class="form-control" rows="5" id="comment"></textarea>

    let element = document.createElement('textarea')
    element.setAttribute('class', params.class)
    element.setAttribute('rows', params.rows)
    element.setAttribute('id', params.id)
    element.setAttribute('name', 'setting['+params.id+']')
    // element.setAttribute('value', object.value)
    element.innerHTML = object.value

    if (!object.options) return [true, element];
    for (const [k, v] of Object.entries(object.options)) {
      element.setAttribute(k, v)
    }
    
    return [true, element]
  }
}

graphic = (typeof graphic === 'undefined') ? new Object() : graphic
graphic.textarea = new Textarea()