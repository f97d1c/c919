class Button {
  renderToButton(object, params){
    let defaultValue = {
      id: undefined,
      class: 'btn btn-primary ml-2',
    }
    params = Object.assign(defaultValue, params)
    if (!object) return [false, '渲染对象(object)不能为空']

    // <button type="button" class="btn btn-primary">主要按钮</button>
    let element = document.createElement('button')
    element.setAttribute('class', params.class)
    element.setAttribute('type', 'button')
    element.setAttribute('value', object.value)
    
    if (object.options){
      for (const [key_, value_] of Object.entries(object.options)) {
        element.setAttribute(key_, value_)
      }
    }

    element.innerHTML = object.name
    return [true, element]
  }
}

graphic = (typeof graphic === 'undefined') ? new Object() : graphic
graphic.button = new Button()