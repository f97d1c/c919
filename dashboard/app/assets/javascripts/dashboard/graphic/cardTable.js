// 致力于通过原生Js生成相关html元素(目前为bootstrap标准)
graphic = (typeof graphic === 'undefined') ? new Object() : graphic
// import * as myModule from '/modules/my-module.js';

class CardTable {
  renderCardTable (object, params) {
    var defaultValue = {
      object: undefined,
      title: undefined,
      divId: undefined,
      divBgClass: 'bg-dark', 
      divClass: 'card text-white float-left',
      rowCount: 4,
      tableClass: 'table table-striped table-hover card-body',
      pureTextContent: true,
      keyWidth: 40,
      ellipsisText: true, // 文本过长内容进行省略
    }

    params = Object.assign(defaultValue, params)
    if (!object) return [false, '渲染对象(object)不能为空']

    var div = document.createElement('div')
    div.setAttribute('class', params.divClass)
    div.className+=(' '+params.divBgClass)
    if (params.rowCount > 1){
      var divWidth = (100-(params.rowCount*0.5))/params.rowCount
      div.setAttribute('style', 'width:'+divWidth+'%; margin-left: 0.25%; margin-right: 0.25%;')
    }else{
      div.setAttribute('style', 'width: 100%;')
    }


    if (params.divId) div.setAttribute('id', params.divId);

    var table = document.createElement('table')
    table.setAttribute('class', params.tableClass)
    table.setAttribute('style', 'margin-bottom: 0;')
    
    if (params.title) {
      var thead = document.createElement('thead')
      var tr = document.createElement('tr')
      var th = document.createElement('th')
      th.setAttribute('class', 'text-center')
      th.setAttribute('colspan', 2)
      th.innerHTML = params.title
      tr.appendChild(th)
      thead.appendChild(tr)
      table.appendChild(thead)
    }

    var tbody = document.createElement('tbody')

    for (let [key, value] of Object.entries(object)) {
      let tr, td, span
      tr = document.createElement('tr')
      td = document.createElement('td')
      td.setAttribute('style', 'word-break:break-all; width: '+params.keyWidth+'%;')
      span = document.createElement('span')
      span.setAttribute('class', 'float-right')
      span.innerHTML = key
      td.appendChild(span)
      tr.appendChild(td)

      td = document.createElement('td')
      td.setAttribute('style', 'word-break:break-all; width: auto;')
      if (!params.pureTextContent){
        td.innerHTML = value
        tr.appendChild(td)
        tbody.appendChild(tr)
        continue
      }

      span = document.createElement('span')
      span.setAttribute('class', 'float-left')
      // 这里按屏幕宽度1600px换算value所在td的px
      let valueWidth
      if(params.ellipsisText) valueWidth = (1600/params.rowCount)*((100-params.keyWidth)/100)
      // 列宽/字号=允许容纳长度 汉字按字号的1.5倍计算实际字号
      if(params.ellipsisText && (value.length > valueWidth/(16*1.5))){
        span.setAttribute('style', 'width:'+valueWidth+'px; white-space:nowrap; overflow:hidden; text-overflow:ellipsis;')
        span.setAttribute('title', value)
        // span.setAttribute('data-placement', 'top')
      }
      span.innerHTML = value
      td.appendChild(span)


      tr.appendChild(td)
      tbody.appendChild(tr)
    }

    table.appendChild(tbody)
    div.appendChild(table)

    return [true, div]
  }
}

graphic.cardTable = new CardTable();
