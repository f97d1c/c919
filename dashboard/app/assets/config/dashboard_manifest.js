//= link_directory ../javascripts/dashboard .js
//= link_directory ../stylesheets/dashboard .css

// 寻找自动写入的方案

// Css
//= link dashboard/bootstrap_4/bootstrap.min.css


// Js
//= link dashboard/jQuery/3_5_1/min.js
//= link dashboard/jQuery/serializejson.js

//= link dashboard/chart_js/chart_js.js

//= link dashboard/bootstrap_4/bootstrap.min.js
//= link dashboard/bootstrap_4/popper.js

//= link dashboard/amcharts/v4/core.js
//= link dashboard/amcharts/v4/charts.js
//= link dashboard/amcharts/v4/animated.js
//= link dashboard/amcharts/v4/zh_Hans.js
//= link dashboard/amcharts/v4/word_cloud.js

//= link dashboard/google_chart/loader.js

//= link dashboard/highcharts/highcharts.js
//= link dashboard/highcharts/timeline.js
//= link dashboard/highcharts/exporting.js
//= link dashboard/highcharts/accessibility.js
//= link dashboard/highcharts/data.js
//= link dashboard/highcharts/heatmap.js
//= link dashboard/highcharts/boost-canvas.js
//= link dashboard/highcharts/boost.js
//= link dashboard/highcharts/zh_CN.js

//= link dashboard/space/core.js
//= link dashboard/space/string.js
//= link dashboard/space/md5.js

//= link dashboard/graphic/cardTable.js
//= link dashboard/graphic/objectTable.js
//= link dashboard/graphic/listTable.js
//= link dashboard/graphic/searchForm.js
//= link dashboard/graphic/input.js
//= link dashboard/graphic/textarea.js
//= link dashboard/graphic/button.js
//= link dashboard/graphic/setting.js

//= link dashboard/ff4c00/header.js
//= link dashboard/ff4c00/footer.js

//= link dashboard/wireway/main.js
