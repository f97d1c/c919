Dashboard::Engine.routes.draw do

  resources :restful, only:[] do 
    collection do
      get :example
      get :exposed_api 
      post :request_api
      get :test
      get :request_api_params
    end 
  end

  match '/restful/request_api' => "restful#request_api", via: :options
end
