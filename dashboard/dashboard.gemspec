$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "dashboard/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "dashboard"
  spec.version     = Dashboard::VERSION
  spec.authors     = ["ff4c00"]
  spec.email       = ["ff4c00@gmail.com"]
  spec.homepage    = "https://gitlab.com/ff4c00/dashboard"
  spec.summary     = "用于数据图形化展示"
  spec.description = "用于数据 列表,图表等 图形化展示用途"
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", "~> 5.2.4", ">= 5.2.4.3"
  # 软删除
  spec.add_dependency "paranoia", "~> 2.4.2"
  # makrdown展示
  spec.add_dependency "commonmarker", "~> 0.19.0"
  # 解析html/xml等文本
  spec.add_dependency 'nokogiri', '~> 1.6', '>= 1.6.3.1'
  # Http请求
  spec.add_dependency "rest-client", '2.1.0'
  # 终端打印字体颜色
  spec.add_dependency "colorize", '0.8.1'
    
  # C919组件组网协作
  spec.add_dependency 'wireway'

  # sass-css预处理器
  spec.add_dependency 'sassc-rails'
  
  spec.add_development_dependency "sqlite3"
  # 断点调试
  spec.add_development_dependency "pry-byebug", "~> 3.9.0"
end
