$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "wireway/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "wireway"
  spec.version     = Wireway::VERSION
  spec.authors     = ["ff4c00"]
  spec.email       = ["ff4c00@gmail.com"]
  spec.homepage    = "https://gitlab.com/ff4c00/space/-/tree/master/%E7%9F%A5%E8%AF%86%E4%BD%93%E7%B3%BB/%E5%BA%94%E7%94%A8%E7%A7%91%E5%AD%A6/%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%A7%91%E5%AD%A6/%E8%BD%AF%E4%BB%B6%E5%B7%A5%E7%A8%8B/Rails/C919/wireway"
  spec.summary     = "用于联接各组件"
  spec.description = "用于反向代理C919各组件,实现组件间组网协作."
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    # spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", "~> 5.2.4", ">= 5.2.4.3"

  # Http请求
  spec.add_dependency "rest-client", '2.1.0'
  
  spec.add_development_dependency "sqlite3"

  # 断点调试
  spec.add_development_dependency "pry-byebug", "~> 3.9.0"
end
