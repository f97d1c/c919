module Wireway
  class RestfulController < ApplicationController

    def exposed_api
      res = Wireway::Core::Request::platforms_exposed_api
      render json: res[1]
    end

    def request_api_params
      return render json: {success: false, result: '平台标识(platform_code)不能为空'} unless params[:platform_code]
      return render json: {success: false, result: '接口标识(api_code)不能为空'} unless params[:api_code]

      res = Wireway::Core::Request::request_api_params(platform_code: params[:platform_code], api_code: params[:api_code])
      return render json: {success: false, result: res[1]} unless res[0]

      res[1][:request_params] << {key: 'platform_code', name: '平台标识', explain: '接口所属平台标识', example: params[:platform_code], default: nil, value_type: 'string', necessary: true}
      render json: res[1]
    end

    def request_api_result
      # TODO 初步了解为rails5的安全机制 去除存在风险 暂时先这样
      args = params.permit!.to_hash.reject!{|key| ["action", "controller"].include?(key)}.deep_symbolize_keys

      res = Wireway::Spark.send(args[:platform_code], args)
      return (render html: res[1]) if res[1].is_a?(String)
      tmp = {success: res[0]}
      tmp.merge!(res[1])
      (tmp.merge!(res[2]) rescue nil) if res[2].present?
      render json: tmp.merge!(tmp)
    end

    def ui
    end

  end
end