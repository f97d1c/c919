Wireway::Engine.routes.draw do
  resources :restful, only:[] do 
    collection do
      get :exposed_api
      get :request_api_params
      post :request_api_result
      get :ui
    end 
  end
end
