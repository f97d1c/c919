module Wireway
  module Core
    module Request
      extend self

      $wireway_node_addresses = 404

      # 后期应由专门组件对于已登记地址信息进行活跃验证,并将可用节点信息回传给Satcom
      # 后期当某一节点调用不通过时将常量赋值为404进行更新
      # url路径不需要动态获取,只有ip和端口是动态的,如果请求路径都不一致,不能认为是合规的节点.
      def node_addresses
        return $wireway_node_addresses unless $wireway_node_addresses == 404

        nodes = ['192.168.8.133:3113']
        route = '/satcom/other/c919/node_addresses.json'
        res = get(addresses: nodes, route: route) do |result|
          hash = JSON.parse(result[1][:body]).deep_symbolize_keys 
          [true, hash]
        end

        return res unless res[0]
        nodes = res[1].map{|key, value| [key, value[:nodes]]}.to_h
        $wireway_node_addresses = [true, nodes]
      end 

      def platforms_exposed_api
        res = node_addresses
        return res unless res[0]
        exposed_api = {}
        
        platforms = {:satcom => ["192.168.8.133:3113"], :dashboard => ['192.168.8.133:3111'], :black_box => ['192.168.8.133:3116']}
        platforms.each do |platform_code, addresses|
          route = "/#{platform_code}/restful/exposed_api"
          res = get(addresses: addresses, route: route) do |result|
            hash = JSON.parse(result[1][:body]).deep_symbolize_keys 
            exposed_api.merge!(hash) if hash.is_a?(Hash)
          end
        end

        [true, exposed_api]
      end

      def request_api_params(platform_code:, api_code:)
        res = node_addresses
        return res unless res[0]
        addresses = res[1][platform_code.to_sym]
        route = "/#{platform_code}/restful/request_api_params"

        get(addresses: addresses, route: route, params: {api_code: api_code}) do |result|
          hash = JSON.parse(result[1][:body]).deep_symbolize_keys 
          [true, hash]
        end

      end

      def dashboard(**args)
        params = {
          response_type: 'json',
        }.merge!(args)
        route = '/dashboard/restful/request_api'
        res = node_addresses
        return res unless res[0]
        post(addresses: res[1][:dashboard], route: route, params: params) do |result|
          return [true, result[1][:body]] if (params[:response_type] != 'json')
          body = JSON.parse(result[1][:body]).deep_symbolize_keys 
          return [false, body[:result]] unless body[:success]
          [true, body[:result]]
        end
      end
      
      def satcom(**args)
        params = {}.merge!(args)
        route = '/satcom/restful/request_api'
        res = node_addresses
        return res unless res[0]
        post(addresses: res[1][:satcom], route: route, params: params) do |result|
          body = JSON.parse(result[1][:body]).deep_symbolize_keys 
          return [false, body[:result], body[:errors]].compact unless body[:success]
          # [true, body[:result]]
          [true, body]
        end
      end

      def wireway(**args)
        params = {
          local: false
        }.merge!(args)
        route = '/wireway/restful/request_api_result'
        res = node_addresses
        return res unless res[0]
        post(addresses: res[1][:wireway], route: route, params: params) do |result|
          body = JSON.parse(result[1][:body]).deep_symbolize_keys 
          return [false, body[:result], body[:errors]].compact unless body[:success]
          [true, body[:result]]
        end
      end

      def black_box(**args)
        params = {}.merge!(args)
        route = '/black_box/restful/request_api'
        res = node_addresses
        return res unless res[0]
        res = post(addresses: res[1][:black_box], route: route, params: params) do |result|
          body = JSON.parse(result[1][:body]).deep_symbolize_keys
          return [false, body[:result], body[:errors]].compact unless body[:success]
          [true, body]
        end
      end

      def get(addresses:, route:, params: {})
        result = round_robin(addresses: addresses) do |address|
          full_url = [address+route, URI.encode_www_form(params)]
          full_url.delete_if{|item| !item.present? }
          RestClient.get(full_url.join("?"))
        end
        return result unless (result[0] && block_given?)
        
        begin
          yield result
        rescue
          return [false, '[Wireway]运行时结果处理异常', {errors: {message: $!.to_s, path: $@}}]
        end
      end 

      def post(addresses:, route:, params: {})
        result = round_robin(addresses: addresses) do |address|
          RestClient.post "#{address}/#{route}", params
        end
        return result unless (result[0] && block_given?)

        begin
          yield result
        rescue
          return [false, '[Wireway]运行时结果处理异常', {errors: {message: $!.to_s, path: $@}}]
        end
      end 

      private
        def round_robin(addresses:)
          res = nil
          addresses.each do |address|
            res = get_response do
              yield address
            end
            # TODO: 后期应该优化判断是服务不可以用还是传递参数问题倒是返回false
            # 或者当一定比例节点均无法正常响应时即判定为参数问题
            break if res[0]
          end 
          res
        end

        def get_response
          begin
            response = yield
            return [false, "[Wireway]响应码异常:#{response.code}", {response: response}] unless (response.code == 200)

            hash = {code: 200}
            res = handle_body_encode(body: response.body)
            return res unless res[0]

            hash.merge!({body: res[1]})
          rescue
            return [false, '[Wireway]请求异常', {errors: {message: $!.to_s, path: $@}}]
          end
          
          [true, hash]
        end 

        def handle_body_encode(body: )
          body = case body.encoding.to_s
          when 'UTF-8', 'ASCII-8BIT'
            body
          else
            body.encode('utf-8','gbk', :undef => :replace, :replace => "?", :invalid => :replace).chars.select{|i| i.valid_encoding?}.join
          end

          [true, body]
        end
    end
  end
end
