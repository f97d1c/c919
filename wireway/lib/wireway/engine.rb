module Wireway
  class Engine < ::Rails::Engine
    isolate_namespace Wireway
    require 'rest-client'
    config.eager_load_paths += [File.expand_path("../../lib/", __dir__)]
    config.eager_load_paths += [File.expand_path("lib/", __dir__)]
  end
end
