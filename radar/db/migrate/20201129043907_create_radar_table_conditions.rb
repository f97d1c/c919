class CreateRadarTableConditions < ActiveRecord::Migration[5.2]
  def change
    create_table :radar_table_conditions do |t|
      t.string :table_name, null: false, comment: '条件对应表名'
      t.index :table_name
      
      t.string :name, null: false, comment: '名称'
      t.index :name

      t.string :show_columns, default: nil, comment: '展示字段'
      t.index :show_columns

      t.string :search, null: false, comment: '字段搜索条件'
      t.index :search

      t.string :ransack, null: false, comment: 'Ransack搜索后缀'
      t.index :ransack

      t.integer :is_default, null: false, default: 0, comment: '是否为默认条件'
      t.index :is_default
      
      t.timestamps
    end
  end
end
