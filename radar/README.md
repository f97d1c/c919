> C919系列组成部分,关系型数据库中数据发现/维护及相关统计分析.

<!-- TOC -->

- [说明](#说明)
  - [迁移](#迁移)
  - [执照](#执照)

<!-- /TOC -->

# 说明

## 迁移

```
rake radar:install:migrations
```

## 执照

根据以下条款,该Gem可作为开源软件使用 [MIT License](https://opensource.org/licenses/MIT).
