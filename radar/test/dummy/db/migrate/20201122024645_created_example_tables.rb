class CreatedExampleTables < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name, null: false, comment: '姓名'
      t.string :gender, null: false, comment: '性别'
      t.date   :birthday, null: false, comment: '出生日期'
      t.integer :age, null: false, comment: '年龄'
      t.string :address, null: false, comment: '住址'
      t.string :id_number, null: false, comment: '身份证号'
      t.timestamps
    end

    create_table :contact_records do |t|
      t.string :name, null: false, comment: '联系方式名称'
      t.string :content, null: false, comment: '内容'
      t.timestamps
    end

    create_table :contact_relations do |t|
      t.integer  :contactabel_id,       comment: "关联对象id"
      t.string   :contactabel_type,     comment: "关联对象类型"
      t.integer  :contact_record_id,       comment: "关联对象id"
      t.timestamps
    end

    add_index :contact_relations, :contactabel_id
    add_index :contact_relations, :contactabel_type
    add_index :contact_relations, :contact_record_id

  end
end
