module Radar
  class TableCondition < ApplicationRecord
    serialize :search, Hash
    serialize :ransack, Hash
    serialize :show_columns, Array

    validates :table_name, :name, :search, :ransack, :is_default, presence: true
  end
end
