module Radar
  module BusinessLogic
    
    module Translation
      extend self

      def human_object_names
        # return @object_names if @object_names

        @object_names = {}
        # Rails.application.eager_load! unless Rails.env.production?
        Rails.configuration.eager_load_namespaces.each(&:eager_load!) unless Rails.env.production?

        ActiveRecord::Base.descendants.each do |model|
          next if model.to_s =~ /^ActiveRecord::/
          next if model.to_s =~ /^ActiveStorage::/
          next if model.abstract_class?
          @object_names.merge!({model.model_name.human => model.to_s.gsub('::', '•')})
        end 

        @object_names
      end

      def human_attributes(object:, locale: :zh)
        translates = {}
        object.attributes.each do |key, value|
          translate = human_attribute(object: object, attribute: key, locale: locale)
          next unless translate
          translates.merge!({translate => value})
        end
        
        translates
      end

      def attributes_info(object:, locale: :zh)
        klass = object.class
        info = {}

        object.attributes.each do |key, value|
          translate = human_attribute(object: object, attribute: key, locale: locale)
          next unless translate
          type = klass.columns_hash[key].type
          info.merge!({key => {value: value, locale_key: translate, type: type}})
        end
        
        info
      end 

      private
        # 返回false存在异意 仅供内部使用
        def human_attribute(object:, attribute:, locale: :zh)
          translate = object.class.human_attribute_name(attribute)
          # case locale.to_sym
          # when :zh, :zh_cn, :zh_CN
          #   return false unless (translate =~/^[\u4e00-\u9fa5]{1,}$/)
          # end
          translate
        end

    end
  end
end