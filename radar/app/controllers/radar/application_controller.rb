module Radar
  class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception

    def page_result(result)
      page_res = lambda{|result| result.page(params[:page_number] || 1).per(params[:page_size] || 100)}.call(result)
      (@max_page_number = page_res.total_pages) if init_load?
      (@result_size = result.size) if init_load?
      page_res
    end

    def init_load?
      init_load = params[:init_load]
      return true unless init_load
      init_load.downcase == 'true'
    end

    def auto_load?
      auto_load = params[:auto_load]
      return false unless auto_load
      auto_load.downcase == 'true'
    end

    def render(*args)
      if auto_load?
        super "shared/radar/_load_item", layout: false
      else
        super
      end 
    end

  end
end
