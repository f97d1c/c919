module Radar
  class ManagesController < ApplicationController
    before_action :initialize_instance_variable
    def index
      @klasses = BusinessLogic::Translation::human_object_names
    end

    def search_form
      render layout: false
    end

    def data_list
      if @show_columns.present?
        select_columns = @show_columns.map{|column| "#{@klass.table_name}.#{column}"}.uniq.join(', ')
        query = @klass.select(select_columns).ransack(handle_ransack)
      else
        query = @klass.ransack(handle_ransack)
      end
      datas = page_result(query.result)

      @unites = []
      datas.each do |object|
        @unites << BusinessLogic::Unite.new(object: object)
      end
      render layout: false
    end

    def object
      @object ||= @klass.new
      @unite = BusinessLogic::Unite.new(object: @object)
    end

    def object_info
      return (render text: 'klass不能为空') unless params[:klass]
      return (render text: 'object_id不能为空') unless params[:object_id]
      @unite = BusinessLogic::Unite.new(object: @object)
      render layout: false
    end

    def object_edit
      object = @object || @klass.new
      @unite = BusinessLogic::Unite.new(object: object)
      render layout: false
    end

    def object_update
      @object ||= @klass.new
      attributes = params[@klass_name].permit!.to_hash
      attributes = attributes.transform_values do |value| 
        (JSON.parse(value.gsub("=>", ":").gsub(":nil,", ":null,")) rescue value)
      end
      msg = nil
      unless (@object.update(attributes) rescue (msg = $!;false))
        return render json: {success: false, message: (msg || @error_messages.call(@object)).to_s}
      end
      render json: {success: true, message: '保存成功', object_id: @object.reload.id}
    end

    def save_condition
      condition = TableCondition.new(table_name: @klass.table_name, name: params[:condition_name])
      condition.search = @search
      condition.ransack = @ransack
      condition.show_columns = @show_columns

      unless condition.valid?
        return render json: {success: false, message: @error_messages.call(condition)}
      end

      unless condition.save
        return render json: {success: false, message: @error_messages.call(condition)}
      end

      render json: {success: true, message: '保存成功', condition_id: condition.id}
    end

    def object_delete
      return render json: {success: false, message: '对象为空'} unless @object.present?
      unless (@object.delete rescue (msg = $!;false))
        return render json: {success: false, message: ((msg ||= nil) || @error_messages.call(@object)).to_s}
      end
      render json: {success: true, message: '删除成功'}
    end

    def download
      if @show_columns.present?
        select_columns = @show_columns.map{|column| "#{@klass.table_name}.#{column}"}.uniq.join(', ')
        query = @klass.select(select_columns).ransack(handle_ransack)
      else
        query = @klass.ransack(handle_ransack)
      end
      datas = page_result(query.result)

      tmp = []
      query.result.each do |result|
        unite = BusinessLogic::Unite.new(object: result)
        tmp << unite.zh_cn
      end

      params = {
        api_code: :file_excel_create, 
        file_name: '虚构人物数据导出',
        sheets: [{name: '虚构人物数据导出', key_values: tmp}]
      }

      # res = Wireway::Spark.black_box(params)
      params.merge!(platform: 'black_box')
      res = Wireway::BusinessLogic::Restful.wireway(params)
      return (render json: {success: false, message: ('接口请求失败')}) unless res[0]

      params = {
        api_code: :file_download_send_file,
        rsa_pub: res[1][:rsa_pub]
      }

      begin
        res = RestClient.post "192.168.8.133:3116/black_box/restful/request_api", params
        unless res.code == 200
          return render json: {success: false, message: '文件处理组件请求失败'}
        end
        file_content = Base64.encode64(res.body)#.force_encoding('utf-8')

        return render json: {
          success: true,
          download: {
            mimetype: "application/excel",
            filename: "导出数据.xls",
            data: file_content,
          }
        }

      rescue => exception
        return render json: {success: false, message: '文件处理组件请求异常'}
      end

      render json: {success: false, message: '未知流程'}
    end

    private

      def handle_ransack
        return {} unless @search && @ransack
        tmp = {}
        @search.each do |key, value|
          tmp.merge!({"#{key}_#{@ransack[key]}" => (JSON.parse((value)) rescue value)})
        end
        tmp
      end 

      def initialize_instance_variable
        @error_messages = lambda do |object| 
          object.errors.messages.to_a.map do |key, value| 
            "#{@klass.human_attribute_name(key)}: #{value.join(', ')}"
          end.join(', ')
        end

        if params[:klass]
          klass = params[:klass].gsub('•', '::')
          @klass = (klass.constantize rescue nil)
          return (render text: 'class不存在') unless @klass
          @klass_name = klass.to_s.gsub('::', '•')
        end

        @search = params[:search] && params[:search].permit!.to_hash
        @ransack = params[:ransack] && params[:ransack].permit!.to_hash
        
        if params[:object_id].present? && @klass
          @object = @klass.find(params[:object_id])
          return (render text: 'object不存在') unless @object
        end

        table_condition_id = 1
        table_condition_id = params[:table_condition_id]
        @show_columns = (((params[:show_columns] || ActionController::Parameters.new).permit!.to_hash)[@klass.table_name] || []).delete_if{|item| !item.present? } if @klass

        @table_condictions = TableCondition.where(table_name: @klass.table_name) if @klass
        condiction = @table_condictions.find(table_condition_id) if table_condition_id && @table_condictions

        if condiction
          @search = condiction.search
          @ransack = condiction.ransack
          @show_columns = condiction.show_columns unless @show_columns.present?
        end

        @show_columns = (@klass.columns.map(&:name) - ['created_at', 'updated_at']) if @klass && @show_columns.blank?
      end 
      
  end
end