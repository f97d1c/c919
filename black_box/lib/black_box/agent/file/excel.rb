module BlackBox
  module Agent
    module File
      module Excel
        extend self

        def exposed_api
          {
            create: create(show_how: true),
          }
        end

        def create(show_how: false, **args)
          method_name = '生成xls文档'
          explain = '根据提供的Json数据生成xls格式excel文档'

          described = Described.new(bind: binding)
          described.set_attribute(key: 'file_name', name: '文件名称', explain: '文件名称', example: '', value_type: :string)
          described.set_attribute(key: 'sheets', name: '表格数据', explain: '由固定元素组成的哈希所构成的数组', example: :public_file, value_type: :array)
          self_described = described.get_described
          return self_described if show_how

          res = Wireway::Standard::inspect_all(ideal: self_described[:attributes], reality: args)
          return res unless res[0]

          excel = Core::File::Excel.new(save_path: 'tmp/black_box', file_name: args[:file_name])
          res = excel.set_sheets(sheets: args[:sheets])
          return res unless res[0]
          res = excel.package
          return res unless res[0]

          rsa = Core::Encryption::Rsa.new
          encrypt_content = rsa.public_encrypt(content: {file_path: res[1]}.to_json)
          encoded_uri = URI.escape(encrypt_content)

          [true, {rsa_pub: encoded_uri}]
        end
        
      end
    end
  end
end