module BlackBox
  module Agent
    # 所有对外提供服务功能应该都由该module负责
    module Public
      extend self

      def exposed_api
        hash = exposed_api_flat_
      end 

      # 查询相关api详情
      def explain_api
        tmp = {}
        exposed_api.each do |key, value|
          tmp.merge!({key => value[:explain]})
        end
        tmp
      end 

      private
        # 所有可调用接口集合
        def exposed_api_
          {
            file: {
              excel: BlackBox::Agent::File::Excel::exposed_api,
              download: BlackBox::Agent::File::Download::exposed_api,
            }
          }
        end

        # 将多层级Hash结构变为浅层
        def exposed_api_flat_
          tmp = {}
          exposed_api_.each do |key, value|
            value.each do |ke, val|
              val.each do |k,v|
                tmp.merge!({"#{key}_#{ke}_#{k}".to_sym => v})
              end
            end 
          end
          tmp
        end 

    end
  end
end