module BlackBox
  module Core
    module Dir
      module Path
        extend self

        def legal_paths
          ['tmp', 'tmp/black_box'].map{|dir| Rails.root.join(dir).to_s}
        end

        def legal?(dir_path:, exist: true)
          reg = Regexp.new(legal_paths.join('|'))
          return [false, '非允许访问路径!'] unless (dir_path =~ reg)
          return [false, '目录不存在'] unless (!exist || ::Dir.exist?(dir_path))
          FileUtils.mkdir_p(dir_path) if (!exist && (!::Dir.exist?(dir_path)))
          return [false, '目录不存在'] unless (::Dir.exist?(dir_path))
          [true, '']
        end

      end
    end
  end
end