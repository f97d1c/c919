module BlackBox
  module Core
    module File
      class Excel
        attr_reader :full_path
        attr_writer :save_path

        def initialize(save_path:, file_name:, encoding: 'UTF-8', mode: 'wb')
          @save_path = Rails.root.join(save_path).to_s
          @file_name = file_name
          @io = StringIO.new
          @io.set_encoding('UTF-8')
          Spreadsheet.client_encoding = encoding
          @book = Spreadsheet::Workbook.new
          @sheets = []
          @mode = mode
          @full_path = "#{@save_path}/#{@file_name}.xls"
        end

        # 表格数据赋值
        def set_sheets(sheets:)
          return [false, '应为数组结构'] unless sheets.is_a?(Array)
          
          tmp = []
          sheets.each_with_index do |hash, index|
            res = check_sheet(sheet: hash)
            return [false, "元素下标: #{index}, #{res[1]}"] unless res[0]
            tmp << res[1]
          end

          @sheets = (@sheets + tmp).uniq
          [true, '']
        end

        # 插入单张表格数据
        def insert_sheet(sheet:, index: -1)
          res = check_sheet(sheet: sheet)
          return res unless res[0]
          @sheets.insert(index, sheet)
          [true, '']
        end

        # 生成表格
        def create_sheet(sheet:)
          res = check_sheet(sheet: sheet)
          return res unless res[0]

          worksheet = @book.create_worksheet(name: sheet[:name])
          key_values = sheet[:key_values]

          # 设置表头
          worksheet.row(0).concat(key_values[0].keys.map(&:to_s))
          format = Spreadsheet::Format.new({
            color: sheet[:header][:color], 
            weight: sheet[:header][:weight], 
            size: sheet[:header][:size],
          })
          worksheet.row(0).default_format = format
          worksheet.row(0).height = sheet[:header][:height]

          # 根据列最大值长度决定列宽
          keys = key_values[0].keys
          (0...keys.size).each do |index|
            hash = key_values.max_by{|hash| hash.values[index].to_s.size}
            max_size = [hash.values[index].to_s.size, keys[index].size].max
            worksheet.column(index).width = (max_size > 10 ? max_size*2 : 15)
          end

          # 写入数据
          key_values.each_with_index do |hash, index|
            index+=1
            worksheet.row(index).concat(hash.values)
          end 
          @book.write @io

          [true, '']
        end 

        # 保存文件
        def save_file
          res = Core::Dir::Path::legal?(dir_path: @save_path, exist: false)
          return res unless res[0]

          file = ::File.open(@full_path, @mode)
          # 写入数据
          file.write(@io.string)
          # 关闭文件
          file.close

          [true, @full_path]
        end

        # 打包文件
        def package
          return [false, "表格数据不能为空"] unless @sheets.present?
          @sheets.each do |sheet|
            res = create_sheet(sheet: sheet)
            return res unless res[0]
          end
          save_file
        end

        private
          def check_sheet(sheet:)
            return [false, "数组元素应由哈希构成"] unless sheet.is_a?(Hash)
            sheet = sheet.deep_symbolize_keys
            return [false, "表格数据(key_values)不能为空"] unless sheet[:key_values].present?
            return [false, "表格数据(key_values)应为数组结构"] unless sheet[:key_values].is_a?(Array)
            return [false, "表格数据(key_values)数组元素应由哈希构成"] unless sheet[:key_values][0].is_a?(Hash)
            return [false, "表格名称(name)不能为空"] unless sheet[:name].present?
            sheet[:header] ||= {color: :blue, weight: :bold, size: 16, height: 25}
            [true, sheet]
          end

      end
    end
  end
end
