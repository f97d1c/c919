module BlackBox
  module Core
    module File
      module Path
        extend self

        def legal?(file_path:)
          return [false, '文件不存在'] unless ::File.exist?(file_path)
          
          legal_paths = Core::Dir::Path::legal_paths
          reg = Regexp.new(legal_paths.join('|'))
          unless (file_path =~ reg)
            return [false, '非允许访问路径!']
          end

          [true, '']
        end

      end
    end
  end
end